var restify = require('restify');
var builder = require('botbuilder');
//var botbuilder_azure = require("botbuilder-azure");
var request = require('sync-request')
var nlu_url = 'http://34.195.142.59:5000'
var nlu_project = 'default'

var service = (message) => {
  var path = nlu_url+"/parse?q="+message+"&project="+nlu_project
  console.log(path)
  return JSON.parse(request('GET', path).getBody('utf8'))
}
var locationDialog = require('botbuilder-location');
// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url);
});

var problemtypes ={
  "flat_tyre": "Flat Tyre",
  "out_of_fuel": "Out of Gas",
  "out_of_battery": "Battery dead"
}

var membershipIds ={
	"AB6475XD": {
		"name": "John Doe",
		"salutation": "Mr",
		"sex": "male",
		"car": "BMW 750d"
	},
	"YTR12398": {
		"name": "Logan Green",
		"salutation": "Mr",
		"sex": "male",
		"car": "BMW X1"
	},
	"QWES8848": {
		"name": "Robert Downey",
		"salutation": "Mr",
		"sex": "male",
		"car": "Ford Mustang"
	},
	"ABCD1234": {
		"name": "Charlotte",
		"salutation": "Ms",
		"sex": "female",
		"car": "BMW X5"
	},
	"YKA89874": {
		"name": "Sophia",
		"salutation": "Ms",
		"sex": "female",
		"car": "AUDI A7"
	}
}

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    openIdMetadata: process.env.BotOpenIdMetadata
});

// Listen for messages from users
server.post('/api/messages', connector.listen());

/*----------------------------------------------------------------------------------------
* Bot Storage: This is a great spot to register the private state storage for your bot.
* We provide adapters for Azure Table, CosmosDb, SQL Azure, or you can implement your own!
* For samples and documentation, see: https://github.com/Microsoft/BotBuilder-Azure
* ---------------------------------------------------------------------------------------- */

//var tableName = 'botdata';
//var azureTableClient = new botbuilder_azure.AzureTableClient(tableName, process.env['AzureWebJobsStorage']);
//var tableStorage = new botbuilder_azure.AzureBotStorage({ gzipData: false }, azureTableClient);
server.get(/.*/, restify.plugins.serveStatic({
    'directory': '.',
    'default': 'index.html'
}));
// Create your bot with a function to receive messages from the user
var bot = new builder.UniversalBot(connector);
bot.library(locationDialog.createLibrary("ArdUw7EPbwOivuZgE8hPx3EsAAcgv9EqoDlp0s7TqHgj77Okx6U4GMTbDwkcYbQU"));
var inMemoryStorage = new builder.MemoryBotStorage();
bot.set('storage', inMemoryStorage);

bot.dialog('/', [
  function (session) {
    session.userData={}
    var dt = new Date();
    var date= dt.getFullYear() + "/" + (dt.getMonth() + 1) + "/" + dt.getDate();
    session.userData.date=date
    //session.sendTyping();
    builder.Prompts.text(session, "Hello... Welcome to AAA. How can I help you?");
  },

  function (session, results) {
      session.userData.problem = results.response;
      //session.sendTyping();

      session.beginDialog('problem')

  },

  function (session, results, next) {
    //session.sendTyping();
      session.send("Will help you on this...")
	  if(session.userData.memberID){next()}
	  else{
      builder.Prompts.text(session,  "Please share your membership ID no?");
	  }

      //builder.Prompts.choice(session, "What type of problem you are facing?", "Flat Tyre|Out Of Fuel|Battery Dead", { listStyle: builder.ListStyle.button })
  },
  function (session, results) {
    //session.sendTyping();

	  if(session.userData.memberID){}
	  else {
		  mod_response=results.response.replace(/ /g,'')
		  console.log(mod_response)
		  session.userData.memberID=mod_response
	  }
	  session.userData.memberID = session.userData.memberID.toUpperCase()
	  if(membershipIds[session.userData.memberID]){

		  var member=membershipIds[session.userData.memberID]
		  session.userData.salutation=member.salutation
		  session.userData.name=member.name
      var car=""
      if(session.userData.car && !session.userData.car.includes("car")){
      car = session.userData.car
    }
    else{
      car =  member.car
      session.userData.car = car
    }
    
		  builder.Prompts.confirm(session, `Welcome ${member.salutation}.${member.name}, Is this for your ${car.toUpperCase()}?`)
	  }
	  else{
		  session.userData={}
		  session.endConversation("Your membership ID is not registered with us. Thanks for contacting us!!")
	  }

  },
  function (session, results, next){
	if(!results.response){
	  session.beginDialog('getOtherPerson')
	}
    else{next()}

  },
  function (session, results, next){
	  if(session.userData.problemtype=="problem"){
      builder.Prompts.choice(session, "What type of problem you are facing?", "Flat Tyre|Out Of Fuel|Battery Dead", { listStyle: builder.ListStyle.button })

	  }
	  else{
		  next()
	  }
  },
  function (session, results){
	  if(results.response){
	 if(results.response.entity=="Flat Tyre"){
		 session.userData.problemtype="flat_tyre"
	 }
	  if(results.response.entity=="Out Of Fuel"){
		 session.userData.problemtype="out_of_fuel"
	 }
	  if(results.response.entity=="Battery Dead"){
		 session.userData.problemtype="out_of_battery"
	 }
	  }
    var options = {
    prompt: "Where are you located? Type or say an address.",
    useNativeControl: true,
    reverseGeocode: true,
    skipFavorites: false,
		skipConfirmationAsk: true,
    requiredFields:
                locationDialog.LocationRequiredFields.streetAddress |
                locationDialog.LocationRequiredFields.locality |
                locationDialog.LocationRequiredFields.region |
                locationDialog.LocationRequiredFields.postalCode |
                locationDialog.LocationRequiredFields.country
    };
    locationDialog.getLocation(session, options);
  },
  function (session, results) {
      var location = results.response;
	  session.userData.location=location
	  if(session.userData.problemtype=="flat_tyre"){
      builder.Prompts.confirm(session,"Got it... "+session.userData.salutation+"." + session.userData.name +
                  `. I have requested a tow truck to be dispatched to the location ${location.streetAddress}, ${location.locality}, ${location.region}, ${location.postalCode}, ${location.country}. Can I help you with anything else?.\n`
                );
	  }
	  if(session.userData.problemtype=="out_of_fuel"){
            session.beginDialog("fuelconfirm")
	  }
	  if(session.userData.problemtype=="out_of_battery"){
      builder.Prompts.confirm(session,"Got it... "+session.userData.salutation+"." + session.userData.name +
                  `. I have requested a person for a jump start to the location ${location.streetAddress}, ${location.locality}, ${location.region}, ${location.postalCode}, ${location.country}. Can I help you with anything else?.\n`
                );
	  }

  },
  function(session, results){
	  if(results.response){
			 session.userData={}
			 session.endConversation("Please say hello to start a new conversation")
		 }
		 else{
			 session.send("Thank your for contacting AAA. For confirmation your transaction history is shown below and lists down the transactions for the AAA member in the current year")
			 var receiptcard = createReceiptCard(session)
			 var msg = new builder.Message(session).addAttachment(receiptcard)
			 session.endConversation(msg)
		 }
  }
]);

bot.dialog('problem', [
    function (session, args) {
        var problem=session.userData.problem
        var nlu_response = service(problem)
		var intent = nlu_response.intent
		console.log(intent.name+","+intent.confidence)
        if((intent.name=="problem" || intent.name == "flat_tyre" || intent.name == "out_of_fuel" || intent.name == "out_of_battery") && intent.confidence>0.7){
           session.userData.problemtype=intent.name
		   if (nlu_response.entities.length > 0) {
              for (var entity of nlu_response.entities) {
                   console.log(entity.entity+":"+entity.value)
                  session.userData[entity.entity] = entity.value
              }
          }
		   session.endDialog()
        }
        else{
          builder.Prompts.text(session, "Sorry.. I didn't understand!!")
        }
    },
	function (session, results) {
		session.userData.problem=results.response
	    session.replaceDialog('problem');
	}

]);

bot.dialog('getOtherPerson', [
     function(session){
		
		
		 	 builder.Prompts.text(session, "Your car model?")
		
	 },
	 function(session, results){

		 session.userData.car=results.response

		 session.endDialog()
	 }
]);

bot.dialog('fuelconfirm', [
     function(session){
	    builder.Prompts.choice(session, "What is the fuel type?", "Petrol|Diesel", { listStyle: builder.ListStyle.button })
	 },
	 function(session, results){
		 session.userData.fueltype=results.response.entity
		 var location= session.userData.location
		 builder.Prompts.confirm(session,"Got it... "+session.userData.salutation+"." + session.userData.name +
                  `. I have requested to fill ${results.response.entity} in your car in the location ${location.streetAddress}, ${location.locality}, ${location.region}, ${location.postalCode}, ${location.country}. Can I help you with anything else?.\n`
                );
	 },
	 function(session, results){
		 if(results.response){
			 session.userData={}
			 session.endConversation("Please say hello to start a new conversation")
		 }
		 else{
		   
			 session.send("Thank your for contacting AAA. For confirmation your transaction history is shown below and lists down the transactions for the AAA member in the current year")
			 var receiptcard = createReceiptCard(session)
			 var msg = new builder.Message(session).addAttachment(receiptcard)
			 session.endConversation(msg)

		 }
	 }



]);


function createReceiptCard(session) {
    return new builder.ReceiptCard(session)
        .title('History')
        .facts([
            builder.Fact.create(session, session.userData.memberID, session.userData.salutation+"."+membershipIds[session.userData.memberID].name + ":")
        ])
        .items([
            builder.ReceiptItem.create(session, session.userData.car.toUpperCase(), problemtypes[session.userData.problemtype]+`(${session.userData.date})`),
            builder.ReceiptItem.create(session, 'AUDI A7', 'Out of Gas(2018/04/23)')
	    
        ]).buttons([
            builder.CardAction.openUrl(session, 'https://www.aaa.com/International/?area=insurance', 'Detailed History')
        ]);
}